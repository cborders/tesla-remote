package productions.moo.teslaremote.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.EnumSet;

public class VehicleResponse
{
	public enum Door
	{
		DRIVER_FRONT,
		DRIVER_REAR,
		PASSENGER_FRONT,
		PASSENGER_REAR,
		FRONT_TRUNK,
		REAR_TRUNK;

		public boolean open;

		Door()
		{
			open = false;
		}
	}

	public EnumSet<Door> getOpenDoors ()
	{
		EnumSet<Door> open = EnumSet.noneOf(Door.class);

		if(driversFront)
		{
			open.add(Door.DRIVER_FRONT);
		}

		if(driversRear)
		{
			open.add(Door.DRIVER_REAR);
		}

		if(passengerFront)
		{
			open.add(Door.PASSENGER_FRONT);
		}

		if(passengerRear)
		{
			open.add(Door.PASSENGER_REAR);
		}

		if(rearTrunk)
		{
			open.add(Door.REAR_TRUNK);
		}

		if(frontTrunk)
		{
			open.add(Door.FRONT_TRUNK);
		}

		return open;
	}

	@SerializedName(value = "df")
	boolean driversFront;

	@SerializedName(value = "dr")
	boolean driversRear;

	@SerializedName(value = "pf")
	boolean passengerFront;

	@SerializedName(value = "pr")
	boolean passengerRear;

	@SerializedName(value = "rt")
	boolean rearTrunk;

	@SerializedName(value = "ft")
	boolean frontTrunk;

	@Expose
	@SerializedName(value = "car_type")
	public String model; //: "s",

	@Expose
	@SerializedName(value = "car_version")
	public String carVersion; //: "2.9.40",

	@Expose
	@SerializedName(value = "locked")
	public boolean locked; //: true,

	@Expose
	@SerializedName(value = "odometer")
	public float odometerReading; //: 41.169065,

	// region Don't know what these are
	@Expose
	@SerializedName(value = "api_version")
	private int api_version; //: 3,

	@Expose
	@SerializedName(value = "calendar_supported")
	private boolean calendar_supported; //: true,

	@Expose
	@SerializedName(value = "center_display_state")
	private int center_display_state; //: 0,

	@Expose
	@SerializedName(value = "dark_rims")
	private boolean dark_rims; //: false,

	@Expose
	@SerializedName(value = "exterior_color")
	private String exterior_color; //: "MetallicBlack",

	@Expose
	@SerializedName(value = "has_spoiler")
	private boolean has_spoiler; //: false,

	@Expose
	@SerializedName(value = "notifications_supported")
	private boolean notifications_supported; //: true,

	@Expose
	@SerializedName(value = "parsed_calendar_supported")
	private boolean parsed_calendar_supported; //: true,

	@Expose
	@SerializedName(value = "perf_config")
	private String perf_config; //: "P2",

	@Expose
	@SerializedName(value = "rear_seat_heaters")
	private int rear_seat_heaters; //: 1,

	@Expose
	@SerializedName(value = "remote_start")
	private boolean remote_start; //: false,

	@Expose
	@SerializedName(value = "remote_start_supported")
	private boolean remote_start_supported; //: true,

	@Expose
	@SerializedName(value = "rhd")
	private boolean rhd; //: false,

	@Expose
	@SerializedName(value = "roof_color")
	private String roof_color; //: "None",

	@Expose
	@SerializedName(value = "seat_type")
	private int seat_type; //: 0,

	@Expose
	@SerializedName(value = "sun_roof_installed")
	private int sun_roof_installed; //: 2,

	@Expose
	@SerializedName(value = "sun_roof_percent_open")
	private int sun_roof_percent_open; //: 0,

	@Expose
	@SerializedName(value = "sun_roof_state")
	private String sun_roof_state; //: "unknown",

	@Expose
	@SerializedName(value = "third_row_seats")
	private String third_row_seats; //: "None",

	@Expose
	@SerializedName(value = "valet_mode")
	private boolean valet_mode; //: false,

	@Expose
	@SerializedName(value = "valet_pin_needed")
	private boolean valet_pin_needed; //: true,

	@Expose
	@SerializedName(value = "vehicle_name")
	private String vehicle_name; //: null,

	@Expose
	@SerializedName(value = "wheel_type")
	private String wheel_type; //: "Super21Gray"
	// endregion
}
