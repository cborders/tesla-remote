package productions.moo.teslaremote.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

public class UserAuthResponse implements Serializable
{
    @Expose
    @SerializedName(value = "access_token")
    public String accessToken;

    @Expose
    @SerializedName(value = "expires_in")
    public long validTime;

    @Expose
    @SerializedName(value = "created_at")
    public Date createdAt;

    public Date getExpiresAt()
    {
        return new Date(createdAt.getTime() + validTime);
    }
}
