package productions.moo.teslaremote.activities;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import de.greenrobot.event.EventBus;
import productions.moo.teslaremote.R;
import productions.moo.teslaremote.api.ChargeState;
import productions.moo.teslaremote.databinding.FragmentChargingBinding;
import productions.moo.teslaremote.events.UpdateEvent;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChargingFragment extends Fragment
{
	private final EventBus _eventBus;
	private FragmentChargingBinding _chargingBinging;

	public ChargingFragment ()
	{
		_eventBus = EventBus.getDefault();
	}

	@Override
	public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		_chargingBinging = DataBindingUtil.inflate(inflater, R.layout.fragment_charging, container, false);
		_chargingBinging.setCharge(new ChargeState());
		View view = _chargingBinging.getRoot();
		_eventBus.registerSticky(this);
		return view;
	}

	@Override
	public void onDestroyView ()
	{
		_eventBus.unregister(this);
		super.onDestroyView();
	}

	public void onEventMainThread(UpdateEvent.ChargeUpdateEvent event)
	{
		_chargingBinging.setCharge(event.charge);
	}
}
