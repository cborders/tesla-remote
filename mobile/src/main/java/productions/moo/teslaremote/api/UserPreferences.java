package productions.moo.teslaremote.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserPreferences
{
    @Expose
    @SerializedName(value = "gui_distance_units")
    public String distanceUnits;

    @Expose
    @SerializedName(value = "gui_temperature_units")
    public String temperatureUnits;

    @Expose
    @SerializedName(value = "gui_charge_rate_units")
    public String chargeRateUnits;

    @Expose
    @SerializedName(value = "gui_24_hour_time")
    public boolean militaryTime;

    @Expose
    @SerializedName(value = "gui_range_display")
    public String rangeDisplay;

}
