package productions.moo.teslaremote.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ObjectResponse<T>
{
    @Expose
    @SerializedName(value = "response")
    public T payload;
}
