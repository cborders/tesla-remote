package productions.moo.teslaremote.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class DriveResponse
{
    @Expose
    @SerializedName(value = "speed")
    public String speed; //: null,

    @Expose
    @SerializedName(value = "latitude")
    public double latitude; //: 39.866006,

    @Expose
    @SerializedName(value = "longitude")
    public double longitude; //: -83.075743,

    @Expose
    @SerializedName(value = "heading")
    public int heading; //: 171,

    @Expose
    @SerializedName(value = "gps_as_of")
    public Date lastGPS; //: 1450326691

    // region Don't know what this is
    @Expose
    @SerializedName(value = "shift_state")
    private String shift_state; //: null,
    // endregion
}
