package productions.moo.teslaremote.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

public class UserAuthRequest implements Serializable
{
    @Expose
    @SerializedName(value = "grant_type")
    public final String grantType = "password";

    @Expose
    @SerializedName(value = "client_id")
    public final String clientId = "e4a9949fcfa04068f59abb5a658f2bac0a3428e4652315490b659d5ab3f35a9e";

    @Expose
    @SerializedName(value = "client_secret")
    public final String clientSecret = "c75f14bbadc8bee3a7594412c31416f8300256d7668ea7e6e7f06727bfb9d220";

    @Expose
    @SerializedName(value = "email")
    public final String email;

    @Expose
    @SerializedName(value = "password")
    public final String password;

    public UserAuthRequest(final String email, final String password)
    {
        this.email = email;
        this.password = password;
    }
}
