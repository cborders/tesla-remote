package productions.moo.teslaremote.api;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import productions.moo.teslaremote.BuildConfig;
import productions.moo.teslaremote.api.deserializers.DateDeserializer;
import productions.moo.teslaremote.utils.StringConstants;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

public class TeslaApiHelper
{
	private static final String BASE_HOST = "owner-api.teslamotors.com";
	private static final String BASE_URL = "https://owner-api.teslamotors.com/";
	private static final String CLIENT_ID = "e4a9949fcfa04068f59abb5a658f2bac0a3428e4652315490b659d5ab3f35a9e";
	private static final String CLIENT_SECRET = "c75f14bbadc8bee3a7594412c31416f8300256d7668ea7e6e7f06727bfb9d220";

	public static final long CONNECT_TIMEOUT_MILLIS = 5000; // 5 Seconds
	public static final long READ_TIMEOUT_MILLIS = 2 * 60 * 1000; // 2 minutes
	public static final long RETRIES = 3;

	private static final String TAG = TeslaApiHelper.class.getSimpleName();

	private final TeslaApi _teslaApi;
	private final SharedPreferences _preferences;

	public TeslaApiHelper(Context context)
	{
		_preferences = PreferenceManager.getDefaultSharedPreferences(context);
		// add our authorization token to all requests
		RequestInterceptor insertTokenInterceptor = new RequestInterceptor()
		{
			@Override
			public void intercept (RequestFacade request)
			{
				if (_preferences.contains(StringConstants.AUTH_TOKEN_KEY))
				{
					request.addHeader("Authorization", "Bearer " + _preferences.getString(StringConstants.AUTH_TOKEN_KEY, ""));
				}
				request.addHeader("Host", BASE_HOST);
				request.addHeader("Content-Type", "application/json");
				request.addHeader("Accept", "application/json");
			}
		};

		Gson gson = new GsonBuilder()
			.registerTypeAdapter(Date.class, new DateDeserializer())
			.excludeFieldsWithoutExposeAnnotation()
			.create();

		RestAdapter.Builder restAdapterBuilder = new RestAdapter.Builder();
		if (BuildConfig.DEBUG)
		{
			restAdapterBuilder.setLogLevel(RestAdapter.LogLevel.FULL);
		}
		else
		{
			restAdapterBuilder.setLogLevel(RestAdapter.LogLevel.NONE);
		}

		OkHttpClient okHttpClient = new OkHttpClient();
		okHttpClient.setConnectTimeout(CONNECT_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS);
		okHttpClient.setReadTimeout(READ_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS);

		okHttpClient.interceptors().add(new Interceptor()
		{
			@Override
			public Response intercept (Chain chain) throws IOException
			{
				Response response = null;
				int tryCount = 0;

				Request request = chain.request();
				RuntimeException lastException = null;

				do
				{
					try
					{
						tryCount++;
						response = chain.proceed(request);
					}
					catch (Exception e)
					{
						Log.d(TAG, "Caught Exception");
						if (e instanceof RuntimeException)
						{
							lastException = (RuntimeException) e;
						}
					}
				}
				while ((response == null || !response.isSuccessful()) && tryCount <= RETRIES);

				if (response == null)
				{
					return null;
				}
				else if (!response.isSuccessful() && lastException != null)
				{
					throw lastException;
				}
				else
				{
					return response;
				}
			}
		});

		RestAdapter restAdapter = restAdapterBuilder.setEndpoint(BASE_URL)
			.setConverter(new GsonConverter(gson))
			.setErrorHandler(new TeslaApiErrorHandler())
			.setClient(new OkClient(okHttpClient))
			.setRequestInterceptor(insertTokenInterceptor)
			.build();

		_teslaApi = restAdapter.create(TeslaApi.class);
	}

	public TeslaApi getTeslaApi ()
	{
		return _teslaApi;
	}
}
