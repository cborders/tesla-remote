package productions.moo.teslaremote.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.util.Date;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;
import productions.moo.teslaremote.R;
import productions.moo.teslaremote.api.ChargeState;
import productions.moo.teslaremote.api.ClimateResponse;
import productions.moo.teslaremote.api.DriveResponse;
import productions.moo.teslaremote.api.ListResponse;
import productions.moo.teslaremote.api.TeslaApi;
import productions.moo.teslaremote.api.TeslaApiHelper;
import productions.moo.teslaremote.api.UserPreferences;
import productions.moo.teslaremote.api.Vehicle;
import productions.moo.teslaremote.api.VehicleResponse;
import productions.moo.teslaremote.events.UpdateEvent;
import productions.moo.teslaremote.utils.StringConstants;

public class MainActivity extends AppCompatActivity
{
	@InjectView(R.id.model)
	TextView _model;

	@InjectView(R.id.name)
	TextView _name;

	@InjectView(R.id.version)
	TextView _version;

	@InjectView(R.id.odometer)
	TextView _odometer;

	@InjectView(R.id.charge)
	ProgressBar _charge;

	@InjectView(R.id.range)
	TextView _range;

	@InjectView(R.id.locked)
	ToggleButton _locked;

	@InjectView(R.id.fragment_pager)
	ViewPager _fragmentPager;

	private TeslaApi _api;

	private SharedPreferences _preferences;

	private EventBus _eventBus;

	@Override
	protected void onCreate (Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		_api = new TeslaApiHelper(this).getTeslaApi();
		_preferences = PreferenceManager.getDefaultSharedPreferences(this);
		_eventBus = EventBus.getDefault();

		if (!_preferences.contains(StringConstants.AUTH_TOKEN_KEY))
		{
			startActivity(new Intent(this, LoginActivity.class));
		}

		setContentView(R.layout.activity_main);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		ButterKnife.inject(this);

		_eventBus.registerSticky(this);
	}

	@Override
	protected void onResume ()
	{
		Date expiration = new Date(_preferences.getLong(StringConstants.AUTH_EXPIRATION_KEY, 0));
		Date now = new Date();
		if (expiration.before(now))
		{
			// Reauth user
			// TODO: Store username and passwords encrypted and use them to reauth
			// For not just make the user sign in again
			startActivity(new Intent(this, LoginActivity.class));
		}
		else
		{
			// Update date from the API
			_eventBus.post(new UpdateStatusEvent());
		}

		_fragmentPager.setAdapter(new TeslaFragmentAdapter(getSupportFragmentManager()));
		super.onResume();
	}

	public void onEventAsync (UpdateStatusEvent event)
	{
		ListResponse<Vehicle> vehicles = _api.getVehicles();

		// TODO: Enable more than one car
		Vehicle vehicle = vehicles.payload.get(0);

		boolean enabled = _api.getMobileEnabled(vehicle.id).payload;

		ChargeState chargeState = _api.getChargeState(vehicle.id).payload;
		_eventBus.postSticky(new UpdateEvent.ChargeUpdateEvent(vehicle, chargeState));

		ClimateResponse climateState = _api.getClimateState(vehicle.id).payload;
		_eventBus.postSticky(new UpdateEvent.ClimateUpdateEvent(vehicle, climateState));

		DriveResponse driveResponse = _api.getDriveState(vehicle.id).payload;
		_eventBus.postSticky(new UpdateEvent.DriveUpdateEvent(vehicle, driveResponse));

		UserPreferences userPrefs = _api.getGuiSettings(vehicle.id).payload;

		VehicleResponse vehicleResponse = _api.getVehicleState(vehicle.id).payload;
		_eventBus.postSticky(new UpdateEvent.VehicleUpdateEvent(vehicle, vehicleResponse));
	}

	public void onEventMainThread (UpdateEvent.ChargeUpdateEvent event)
	{
		_charge.setProgress(event.charge.usableBatteryLevel);
		_range.setText(getString(R.string.mile_format, event.charge.ratedRange));
	}

	public void onEventMainThread (UpdateEvent.VehicleUpdateEvent event)
	{
		_model.setText(getString(R.string.model_format, event.vehicleUpdate.model.toUpperCase()));
		_name.setText(getString(R.string.name_format, event.vehicle.displayName));
		_version.setText(event.vehicleUpdate.carVersion);

		_odometer.setText(getString(R.string.mile_format, event.vehicleUpdate.odometerReading));

		_locked.setChecked(event.vehicleUpdate.locked);
	}

	private class UpdateStatusEvent
	{

	}

	private static class TeslaFragmentAdapter extends FragmentPagerAdapter
	{
		private final FragmentManager _fragmentManager;

		private DoorsFragment _doorFragment;
		private ChargingFragment _chargingFragment;
		private ClimateFragment _climateFragment;

		public TeslaFragmentAdapter (FragmentManager fm)
		{
			super(fm);
			_fragmentManager = fm;
		}

		enum Pages
		{
			Doors,
			Charge,
			Climate//,
//			Map
		}

		@Override
		public Fragment getItem (int position)
		{
			Pages page = Pages.values()[position];

			switch (page)
			{
				case Doors:
				{
					if(_doorFragment == null)
					{
						_doorFragment = new DoorsFragment();
					}

					return _doorFragment;
				}
				case Charge:
				{
					if(_chargingFragment == null)
					{
						_chargingFragment = new ChargingFragment();
					}

					return _chargingFragment;
				}
				case Climate:
				{
					if(_climateFragment == null)
					{
						_climateFragment = new ClimateFragment();
					}

					return _climateFragment;
				}
//				case Map:
//				{
//					if(_doorFragment == null)
//					{
//						_doorFragment = new DoorsFragment();
//					}
//
//					return _doorFragment;
//				}
				default:
				{
					Log.e("MainActivity", "Unknown Page: " + page.toString());
				}
				break;
			}

			return null;
		}

		@Override
		public int getCount ()
		{
			return Pages.values().length;
		}
	}
}
