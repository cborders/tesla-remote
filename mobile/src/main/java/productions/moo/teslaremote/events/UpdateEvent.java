package productions.moo.teslaremote.events;

import productions.moo.teslaremote.api.ChargeState;
import productions.moo.teslaremote.api.ClimateResponse;
import productions.moo.teslaremote.api.DriveResponse;
import productions.moo.teslaremote.api.Vehicle;
import productions.moo.teslaremote.api.VehicleResponse;

public class UpdateEvent
{
	public Vehicle vehicle;

	public UpdateEvent(Vehicle vehicle)
	{
		this.vehicle = vehicle;
	}

	public static class ChargeUpdateEvent extends UpdateEvent
	{
		public ChargeState charge;
		public ChargeUpdateEvent(Vehicle vehicle, ChargeState charge)
		{
			super(vehicle);
			this.charge = charge;
		}
	}

	public static class ClimateUpdateEvent extends UpdateEvent
	{
		public ClimateResponse climate;
		public ClimateUpdateEvent(Vehicle vehicle, ClimateResponse climate)
		{
			super(vehicle);
			this.climate = climate;
		}
	}

	public static class DriveUpdateEvent extends UpdateEvent
	{
		public DriveResponse drive;
		public DriveUpdateEvent(Vehicle vehicle, DriveResponse drive)
		{
			super(vehicle);
			this.drive = drive;
		}
	}

	public static class VehicleUpdateEvent extends UpdateEvent
	{
		public VehicleResponse vehicleUpdate;
		public VehicleUpdateEvent(Vehicle vehicle, VehicleResponse vehicleUpdate)
		{
			super(vehicle);
			this.vehicleUpdate = vehicleUpdate;
		}
	}
}
