package productions.moo.teslaremote.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;

import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import productions.moo.teslaremote.R;
import productions.moo.teslaremote.api.TeslaApi;
import productions.moo.teslaremote.api.TeslaApiHelper;
import productions.moo.teslaremote.api.UserAuthRequest;
import productions.moo.teslaremote.api.UserAuthResponse;
import productions.moo.teslaremote.utils.StringConstants;

public class LoginActivity extends AppCompatActivity
{
	private UserLoginTask _authTask = null;

	// UI references.
	private AutoCompleteTextView _emailView;
	private EditText _passwordView;
	private View _progressView;
	private View _loginFormView;

	private TeslaApi _api;
	private SharedPreferences _preferences;

	@Override
	protected void onCreate (Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		// Set up the login form.
		_emailView = (AutoCompleteTextView) findViewById(R.id.email);

		_passwordView = (EditText) findViewById(R.id.password);
		_passwordView.setOnEditorActionListener(new TextView.OnEditorActionListener()
		{
			@Override
			public boolean onEditorAction (TextView textView, int id, KeyEvent keyEvent)
			{
				if (id == R.id.login || id == EditorInfo.IME_NULL)
				{
					attemptLogin();
					return true;
				}
				return false;
			}
		});

		Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
		mEmailSignInButton.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick (View view)
			{
				attemptLogin();
			}
		});

		_loginFormView = findViewById(R.id.login_form);
		_progressView = findViewById(R.id.login_progress);

		_api = new TeslaApiHelper(this).getTeslaApi();
		_preferences = PreferenceManager.getDefaultSharedPreferences(this);
	}

	/**
	 * Attempts to sign in or register the account specified by the login form.
	 * If there are form errors (invalid email, missing fields, etc.), the
	 * errors are presented and no actual login attempt is made.
	 */
	private void attemptLogin ()
	{
		if (_authTask != null)
		{
			return;
		}

		// Reset errors.
		_emailView.setError(null);
		_passwordView.setError(null);

		// Store values at the time of the login attempt.
		String email = _emailView.getText().toString();
		String password = _passwordView.getText().toString();

		boolean cancel = false;
		View focusView = null;

		// Check for a valid password, if the user entered one.
		if (TextUtils.isEmpty(password))
		{
			_passwordView.setError(getString(R.string.error_invalid_password));
			focusView = _passwordView;
			cancel = true;
		}

		// Check for a valid email address.
		if (TextUtils.isEmpty(email))
		{
			_emailView.setError(getString(R.string.error_field_required));
			focusView = _emailView;
			cancel = true;
		}

		if (cancel)
		{
			// There was an error; don't attempt login and focus the first
			// form field with an error.
			focusView.requestFocus();
		}
		else
		{
			// Show a progress spinner, and kick off a background task to
			// perform the user login attempt.
			showProgress(true);
			_authTask = new UserLoginTask(email, password);
			_authTask.execute((Void) null);
		}
	}

	/**
	 * Shows the progress UI and hides the login form.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	private void showProgress (final boolean show)
	{
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2)
		{
			int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

			_loginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
			_loginFormView.animate().setDuration(shortAnimTime).alpha(
				show ? 0 : 1).setListener(new AnimatorListenerAdapter()
			{
				@Override
				public void onAnimationEnd (Animator animation)
				{
					_loginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
				}
			});

			_progressView.setVisibility(show ? View.VISIBLE : View.GONE);
			_progressView.animate().setDuration(shortAnimTime).alpha(
				show ? 1 : 0).setListener(new AnimatorListenerAdapter()
			{
				@Override
				public void onAnimationEnd (Animator animation)
				{
					_progressView.setVisibility(show ? View.VISIBLE : View.GONE);
				}
			});
		}
		else
		{
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			_progressView.setVisibility(show ? View.VISIBLE : View.GONE);
			_loginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}

	/**
	 * Represents an asynchronous login/registration task used to authenticate
	 * the user.
	 */
	public class UserLoginTask extends AsyncTask<Void, Void, Boolean>
	{
		private final String _email;
		private final String _password;

		UserLoginTask (String email, String password)
		{
			_email = email;
			_password = password;
		}

		@Override
		protected Boolean doInBackground (Void... params)
		{
			try
			{
				UserAuthResponse response = _api.authenticateUser(new UserAuthRequest(_email, _password));
				SharedPreferences.Editor editor = _preferences.edit();
				editor.putString(StringConstants.AUTH_TOKEN_KEY, response.accessToken);
				editor.putLong(StringConstants.AUTH_EXPIRATION_KEY, response.getExpiresAt().getTime());
				editor.commit();
				return true;
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}

			return false;
		}

		@Override
		protected void onPostExecute (final Boolean success)
		{
			_authTask = null;
			showProgress(false);

			if (success)
			{
				finish();
			}
			else
			{
				_passwordView.setError(getString(R.string.error_incorrect_password));
				_passwordView.requestFocus();
			}
		}

		@Override
		protected void onCancelled ()
		{
			_authTask = null;
			showProgress(false);
		}
	}
}

