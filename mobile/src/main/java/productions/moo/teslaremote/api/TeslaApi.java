package productions.moo.teslaremote.api;

import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

public interface TeslaApi
{
    @POST("/oauth/token")
    UserAuthResponse authenticateUser(@Body UserAuthRequest authRequest);

    @GET("/api/1/vehicles")
    ListResponse<Vehicle> getVehicles();

    @GET("/api/1/vehicles/{id}/mobile_enabled")
    ObjectResponse<Boolean> getMobileEnabled(@Path("id") long id);

    @GET("/api/1/vehicles/{id}/data_request/charge_state")
    ObjectResponse<ChargeState> getChargeState(@Path("id") long id);

    @GET("/api/1/vehicles/{id}/data_request/climate_state")
    ObjectResponse<ClimateResponse> getClimateState(@Path("id") long id);

    @GET("/api/1/vehicles/{id}/data_request/drive_state")
    ObjectResponse<DriveResponse> getDriveState(@Path("id") long id);

    @GET("/api/1/vehicles/{id}/data_request/gui_settings")
    ObjectResponse<UserPreferences> getGuiSettings(@Path("id") long id);

    @GET("/api/1/vehicles/{id}/data_request/vehicle_state")
    ObjectResponse<VehicleResponse> getVehicleState(@Path("id") long id);
}
