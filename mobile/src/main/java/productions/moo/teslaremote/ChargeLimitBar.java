package productions.moo.teslaremote;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.util.AttributeSet;
import android.widget.SeekBar;

public class ChargeLimitBar extends SeekBar
{
	private Drawable _progressDrawable;

	private Drawable _thumbDrawable;

	public ChargeLimitBar (Context context)
	{
		super(context);
	}

	public ChargeLimitBar (Context context, AttributeSet attrs)
	{
		super(context, attrs);
	}

	public ChargeLimitBar (Context context, AttributeSet attrs, int defStyleAttr)
	{
		super(context, attrs, defStyleAttr);
	}

	public void setLimit (int limit)
	{
		super.setProgress(limit);
	}

	public int getLimit ()
	{
		return super.getProgress();
	}

	public void setCharge (int charge)
	{
		super.setSecondaryProgress(charge);
	}

	public int getCharge ()
	{
		return super.getSecondaryProgress();
	}

	@Override
	protected synchronized void onDraw (Canvas canvas)
	{
//		super.onDraw(canvas);
		drawTrack(canvas);
		drawThumb(canvas);
	}

	/**
	 * Draws the progress bar track.
	 */
	void drawTrack (Canvas canvas)
	{
		if (_progressDrawable == null)
		{
			_progressDrawable = getProgressDrawable();
		}

		if (_progressDrawable != null)
		{
			// Translate canvas so a indeterminate circular progress bar with padding
			// rotates properly in its animation
			final int saveCount = canvas.save();

			if (_progressDrawable instanceof LayerDrawable)
			{
				Drawable secondary = ((LayerDrawable)_progressDrawable).findDrawableByLayerId(android.R.id.secondaryProgress);
				secondary.setLevel(0);

				Drawable progress = ((LayerDrawable)_progressDrawable).findDrawableByLayerId(android.R.id.progress);
				float scale = getMax() > 0 ? (float) getSecondaryProgress() / (float) getMax() : 0;
				final int level = (int) (scale * 10000);
				progress.setLevel(level);
			}

			canvas.translate(getPaddingLeft(), getPaddingTop());

			_progressDrawable.draw(canvas);
			canvas.restoreToCount(saveCount);
		}
	}

	/**
	 * Draw the thumb.
	 */
	void drawThumb (Canvas canvas)
	{
		if (_thumbDrawable == null)
		{
			_thumbDrawable = getThumb();
		}

		if (_thumbDrawable != null)
		{
			canvas.save();
			// Translate the padding. For the x, we need to allow the thumb to
			// draw in its extra space
			canvas.translate(getPaddingLeft() - getThumbOffset(), getPaddingTop());
			_thumbDrawable.draw(canvas);
			canvas.restore();
		}
	}
}
