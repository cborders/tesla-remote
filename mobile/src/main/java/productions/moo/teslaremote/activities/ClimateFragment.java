package productions.moo.teslaremote.activities;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import productions.moo.teslaremote.R;
import productions.moo.teslaremote.api.ChargeState;
import productions.moo.teslaremote.api.ClimateResponse;
import productions.moo.teslaremote.databinding.FragmentClimateBinding;
import productions.moo.teslaremote.events.UpdateEvent;

/**
 * A simple {@link Fragment} subclass.
 */
public class ClimateFragment extends Fragment
{
	@InjectView(R.id.front_defrost)
	ImageButton _frontDefrost;

	@InjectView(R.id.rear_defrost)
	ImageButton _rearDefrost;

	@InjectView(R.id.driver_seat)
	ImageButton _driverSeat;

	@InjectView(R.id.passenger_seat)
	ImageButton _passengerSeat;

	@InjectView(R.id.rear_left)
	ImageButton _rearLeft;

	@InjectView(R.id.rear_center)
	ImageButton _rearCenter;

	@InjectView(R.id.rear_right)
	ImageButton _rearRight;

	@InjectView(R.id.back_left)
	ImageButton _backLeft;

	@InjectView(R.id.back_right)
	ImageButton _backRight;

	private final EventBus _eventBus;

	private FragmentClimateBinding _dataBinding;

	public ClimateFragment ()
	{
		_eventBus = EventBus.getDefault();
	}

	@Override
	public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		_dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_climate, container, false);
		_dataBinding.setClimate(new ClimateResponse());
		View view = _dataBinding.getRoot();

		ButterKnife.inject(this, view);
		_eventBus.registerSticky(this);

		return view;
	}

	@Override
	public void onDestroyView ()
	{
		_eventBus.unregister(this);
		super.onDestroyView();
	}

	@OnClick({ R.id.driver_up, R.id.driver_down })
	public void adjustDriverSettings (ImageButton src)
	{

	}

	@OnClick({ R.id.passenger_up, R.id.passenger_down })
	public void adjustPassengerSettings (ImageButton src)
	{

	}

	@OnClick({ R.id.front_defrost, R.id.driver_seat })
	public void adjustDefroster (ImageButton src)
	{

	}

	@OnClick({ R.id.passenger_seat, R.id.rear_left, R.id.rear_center, R.id.rear_right, R.id.back_left, R.id.back_right, R.id.rear_defrost })
	public void adjustSeats (ImageButton src)
	{

	}

	public void onEventMainThread (UpdateEvent.ClimateUpdateEvent event)
	{
		_dataBinding.setClimate(event.climate);

		_frontDefrost.getDrawable().setLevel(event.climate.frontDefrost);
		_rearDefrost.getDrawable().setLevel(event.climate.rearDefrostOn ? 1 : 0);
		_driverSeat.getDrawable().setLevel(event.climate.seatHeaterDriver);
		_passengerSeat.getDrawable().setLevel(event.climate.seatHeaterPassenger);
		_rearLeft.getDrawable().setLevel(event.climate.seatHeaterRearLeft);
		_rearCenter.getDrawable().setLevel(event.climate.seatHeaterRearRight);
		_rearRight.getDrawable().setLevel(event.climate.seatHeaterRearCenter);
		_backLeft.getDrawable().setLevel(event.climate.seatHeaterRearLeftBack);
		_backRight.getDrawable().setLevel(event.climate.seatHeaterRearRightBack);
	}
}
