package productions.moo.teslaremote.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

import productions.moo.teslaremote.R;

public class ChargeState
{
    public enum State implements Serializable
    {
        @SerializedName("Disconnected")
        DISCONNECTED(R.string.disconnected),
        @SerializedName("Connected")
        CONNECTED(R.string.connected),
        @SerializedName("Starting")
        STARTING(R.string.starting),
        @SerializedName("Charging")
        CHARGING(R.string.charging),
        @SerializedName("Stopped")
        STOPPED(R.string.stopped),
        @SerializedName("Complete")
        COMPLETE(R.string.complete),
        UNKNOWN(R.string.unknown);

		public int nameRes;
		State(int nameRes)
		{
			this.nameRes = nameRes;
		}
    }

    public enum ChargePortLatchState implements Serializable
    {
        @SerializedName("Engaged")
        ENGAGED,
        UNKOWN
    }

    public enum FastChargerType implements Serializable
    {
        @SerializedName("<invalid>")
        INVALID,
        UNKNOWN
    }

    @Expose
    @SerializedName(value = "charging_state")
    public State state = State.UNKNOWN;

    // region User Settings
    @Expose
    @SerializedName(value = "charge_limit_soc")
    public int chargeLimit;

    @Expose
    @SerializedName(value = "charge_limit_soc_std")
    public int standardChargeLimit;

    @Expose
    @SerializedName(value = "charge_limit_soc_min")
    public int minChargeLimit;

    @Expose
    @SerializedName(value = "charge_limit_soc_max")
    public int maxChargeLimit;

    @Expose
    @SerializedName(value = "charge_to_max_range")
    public boolean chargeToMax;
    // endregion

    // region Vehicle State
    @Expose
    @SerializedName(value = "charge_port_door_open")
    public boolean chargePortOpen;

    @Expose
    @SerializedName(value = "motorized_charge_port")
    public boolean motorizedChargePort;

    @Expose
    @SerializedName(value = "eu_vehicle")
    public boolean euVehicle;

    @Expose
    @SerializedName(value = "charge_port_latch")
    public ChargePortLatchState chargePortLatchState;

    @Expose
    @SerializedName(value = "battery_heater_on")
    public boolean batteryHeaterOn;
    // endregion

    // region Schedule
    @Expose
    @SerializedName(value = "scheduled_charging_start_time")
    public Date scheduledChargingStart;

    @Expose
    @SerializedName(value = "scheduled_charging_pending")
    public boolean scheduledChargingPending;
    // endregion

    // region Range
    /** Miles of range on the current charge level */
    @Expose
    @SerializedName(value = "battery_range")
    public float ratedRange;

    /** Ideal miles of range on the current charge level */
    @Expose
    @SerializedName(value = "ideal_battery_range")
    public float idealRange;

    /** Miles of range estimated from recent driving. */
    @Expose
    @SerializedName(value = "est_battery_range")
    private float estimatedRange;
    // endregion

    // region Battery Level
    /** Battery level as a percentage */
    @Expose
    @SerializedName(value = "battery_level")
    public int batteryLevel;

    /** Usable battery level as a percentage */
    @Expose
    @SerializedName(value = "usable_battery_level")
    public int usableBatteryLevel;

    /** Current flowing into the battery */
    @Expose
    @SerializedName(value = "battery_current")
    private float batteryCurrent;
    // endregion

    // region Charger Stats
    @Expose
    @SerializedName(value = "fast_charger_present")
    public boolean fastChargerPresent;

    @Expose
    @SerializedName(value = "fast_charger_type")
    public FastChargerType fastChargerType;

    @Expose
    @SerializedName(value = "charger_voltage")
    public float chargerVoltage;

    /** Max current allowed by charger & adapter */
    @Expose
    @SerializedName(value = "charger_pilot_current")
    public float chargerPilotCurrent;

	/** Current requested by adapter */
	@Expose
	@SerializedName(value = "charge_current_request")
	public float chargeCurrentRequest;

	/** Current actually supplied by charger */
    @Expose
    @SerializedName(value = "charger_actual_current")
    public float chargerActualCurrent;

	/** Max requested current */
    @Expose
    @SerializedName(value = "charge_current_request_max")
    public int chargeCurrentRequestMax;

	/** Charger voltage */
    @Expose
    @SerializedName(value = "charger_power")
    public float chargerPower;

    /** Miles added per hour of charge */
    @Expose
    @SerializedName(value = "charge_rate")
    public float chargeRate;

    /** Hours until fully charged */
    @Expose
    @SerializedName(value = "time_to_full_charge")
    public float timeToFull;
    // endregion

    // region Don't know what these are
    @Expose
    @SerializedName(value = "not_enough_power_to_heat")
    private boolean notEnoughPowerToHeat; //: false,

    @Expose
    @SerializedName(value = "max_range_charge_counter")
    private int maxRangeChargeCounter; //: 0,

    @Expose
    @SerializedName(value = "charge_energy_added")
    private float charge_energy_added; //: 0,

    @Expose
    @SerializedName(value = "charge_miles_added_rated")
    private float charge_miles_added_rated; //: 0,

    @Expose
    @SerializedName(value = "charge_miles_added_ideal")
    private float charge_miles_added_ideal; //: 0,

    @Expose
    @SerializedName(value = "trip_charging")
    private boolean trip_charging; //: false,

    @Expose
    @SerializedName(value = "user_charge_enable_request")
    private String user_charge_enable_request; //: null,

    @Expose
    @SerializedName(value = "charge_enable_request")
    private boolean charge_enable_request; //: true,

    @Expose
    @SerializedName(value = "charger_phases")
    private String charger_phases; //: null,

    @Expose
    @SerializedName(value = "managed_charging_active")
    private boolean managed_charging_active; //: false,

    @Expose
    @SerializedName(value = "managed_charging_user_canceled")
    private boolean managed_charging_user_canceled; //: false,

    @Expose
    @SerializedName(value = "managed_charging_start_time")
    private Date managed_charging_start_time; //: null
}
