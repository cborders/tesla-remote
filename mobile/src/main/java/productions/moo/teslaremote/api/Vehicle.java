package productions.moo.teslaremote.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

public class Vehicle
{
    @Expose
    @SerializedName(value = "color")
    public String color; //:null,

    @Expose
    @SerializedName(value = "display_name")
    public String displayName; //:"118998",

    @Expose
    @SerializedName(value = "id")
    public long id; //:37296188704254943,

    @Expose
    @SerializedName(value = "option_codes")
    public String optionCodes; //:"MS04,RENA,AU01,BC0R,BP00,BR01,BS00,CDM0,CH00,PMBL,CW02,DA02,DCF0,DRLH,DSH7,DV4W,FG02,HP00,IDHG,IX01,LP01,ME02,MI00,PA00,PF01,PI01,PK00,PS01,PX00,PX4D,QXMB,RFP2,SC01,SP00,SR01,SU01,TM00,TP03,TR00,UTAB,WTSG,WTX0,X001,X003,X007,X011,X013,X021,X024,X027,X028,X031,X037,X040,YF01,COUS",

    @Expose
    @SerializedName(value = "vin")
    public String vin; //:"5YJSA1E47FF118998",

    @Expose
    @SerializedName(value = "remote_start_enabled")
    public boolean remoteStartEnabled; //:true,

    @Expose
    @SerializedName(value = "calendar_enabled")
    public boolean calendarEnabled; //:true,

    @Expose
    @SerializedName(value = "notifications_enabled")
    public boolean notificationsEnabled; //:true,

	// region No idea what these do!
	@Expose
	@SerializedName(value = "vehicle_id")
	public long vehicleId; //:1730158980,

	@Expose
	@SerializedName(value = "state")
	public String state; //:"online",

	@Expose
	@SerializedName(value = "id_s")
	public String idS; //:"37296188704254943",

    @Expose
    @SerializedName(value = "backseat_token")
    public String backseatToken; //:null,

    @Expose
    @SerializedName(value = "backseat_token_updated_at")
    public Date backseatTokenUpdatedAt; //:null

    @Expose
    @SerializedName(value = "tokens")
    public List<String> tokens;
	// endregion
}
