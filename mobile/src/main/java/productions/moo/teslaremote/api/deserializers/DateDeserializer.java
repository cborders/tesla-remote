package productions.moo.teslaremote.api.deserializers;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.Date;

public class DateDeserializer implements JsonDeserializer
{
	@Override
	public Object deserialize (JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException
	{
		long time = json.getAsLong() * 1000;
		return new Date(time);
	}
}
