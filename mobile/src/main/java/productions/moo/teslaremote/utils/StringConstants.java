package productions.moo.teslaremote.utils;

public class StringConstants
{
	public static final String AUTH_TOKEN_KEY = "AUTH_TOKEN_KEY";
	public static final String AUTH_EXPIRATION_KEY = "AUTH_EXPIRATION_KEY";
}
