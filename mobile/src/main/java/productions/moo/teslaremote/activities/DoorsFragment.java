package productions.moo.teslaremote.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.EnumSet;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;
import productions.moo.teslaremote.R;
import productions.moo.teslaremote.api.VehicleResponse;
import productions.moo.teslaremote.events.UpdateEvent;

/**
 * A simple {@link Fragment} subclass.
 */
public class DoorsFragment extends Fragment
{
	@InjectView(R.id.driver_front)
	TextView _driverFront;

	@InjectView(R.id.driver_rear)
	TextView _driverRear;

	@InjectView(R.id.pass_front)
	TextView _passFront;

	@InjectView(R.id.pass_rear)
	TextView _passRear;

	@InjectView(R.id.front_trunk)
	TextView _frontTrunk;

	@InjectView(R.id.rear_trunk)
	TextView _rearTrunk;


	private EventBus _eventBus;

	public DoorsFragment ()
	{
		_eventBus = EventBus.getDefault();
	}

	@Override
	public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.fragment_doors, container, false);
		ButterKnife.inject(this, view);

		_eventBus.registerSticky(this);

		return view;
	}

	@Override
	public void onDestroyView ()
	{
		_eventBus.unregister(this);
		super.onDestroyView();
	}

	public void onEventMainThread(UpdateEvent.VehicleUpdateEvent event)
	{
		EnumSet<VehicleResponse.Door> openDoors = event.vehicleUpdate.getOpenDoors();

		_driverFront.setText(openDoors.contains(VehicleResponse.Door.DRIVER_FRONT) ? "Open" : "Closed");
		_driverRear.setText(openDoors.contains(VehicleResponse.Door.DRIVER_REAR) ? "Open" : "Closed");
		_passFront.setText(openDoors.contains(VehicleResponse.Door.PASSENGER_FRONT) ? "Open" : "Closed");
		_passRear.setText(openDoors.contains(VehicleResponse.Door.PASSENGER_REAR) ? "Open" : "Closed");
		_frontTrunk.setText(openDoors.contains(VehicleResponse.Door.FRONT_TRUNK) ? "Open" : "Closed");
		_rearTrunk.setText(openDoors.contains(VehicleResponse.Door.REAR_TRUNK) ? "Open" : "Closed");
	}
}
