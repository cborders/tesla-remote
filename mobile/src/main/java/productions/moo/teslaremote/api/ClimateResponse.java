package productions.moo.teslaremote.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ClimateResponse
{
    @Expose
    @SerializedName(value = "inside_temp")
    public float insideTemp; //: 21.4,

    @Expose
    @SerializedName(value = "outside_temp")
    public float outsideTemp; //: 17.5,

    @Expose
    @SerializedName(value = "driver_temp_setting")
    public float driverTemp; //: 19.2,

    @Expose
    @SerializedName(value = "passenger_temp_setting")
    public float passengerTemp; //: 19.2,

    @Expose
    @SerializedName(value = "is_auto_conditioning_on")
    public boolean is_auto_conditioning_on; //: false,

    @Expose
    @SerializedName(value = "is_front_defroster_on")
    public int frontDefrost; //: 3,

    @Expose
    @SerializedName(value = "is_rear_defroster_on")
    public boolean rearDefrostOn; //: false,

    @Expose
    @SerializedName(value = "fan_status")
    public int fan_status; //: 0,

    @Expose
    @SerializedName(value = "seat_heater_left")
    public int seatHeaterDriver; //: 0,

    @Expose
    @SerializedName(value = "seat_heater_right")
    public int seatHeaterPassenger; //: 0,

    @Expose
    @SerializedName(value = "seat_heater_rear_left")
    public int seatHeaterRearLeft; //: 0,

    @Expose
    @SerializedName(value = "seat_heater_rear_right")
    public int seatHeaterRearRight; //: 0,

    @Expose
    @SerializedName(value = "seat_heater_rear_center")
    public int seatHeaterRearCenter; //: 0,

    @Expose
    @SerializedName(value = "seat_heater_rear_right_back")
    public int seatHeaterRearRightBack; //: 0,

    @Expose
    @SerializedName(value = "seat_heater_rear_left_back")
    public int seatHeaterRearLeftBack; //: 0,

    @Expose
    @SerializedName(value = "smart_preconditioning")
    public boolean smart_preconditioning; //: false

}
