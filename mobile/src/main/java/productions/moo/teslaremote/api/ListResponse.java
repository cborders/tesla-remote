package productions.moo.teslaremote.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ListResponse<T>
{
    @Expose
    @SerializedName(value = "response")
    public List<T> payload;
}
